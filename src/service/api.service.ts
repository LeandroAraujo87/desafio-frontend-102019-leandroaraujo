import { Injectable } from '@angular/core';
import { Observable, of, throwError } from 'rxjs';
import { HttpClient, HttpHeaders, HttpErrorResponse } from '@angular/common/http';
import { catchError, tap, map } from 'rxjs/operators';
import { Car } from 'src/model/car';

const httpOptions = {
  headers: new HttpHeaders({'Content-Type': 'application/json'})
};
const apiUrl = 'http://localhost:9090/api/car';

@Injectable({
  providedIn: 'root'
})
export class ApiService {

  constructor(private http: HttpClient) { }

  getCars(): Observable<Car[]> {
    const url = `${apiUrl}`;
    return this.http.get<Car[]>(url)
      .pipe(
        tap(cars => console.log('Read cars')),
        catchError(this.handleError('getCars', []))
      );
  }

  getCarByCategory(category: number): Observable<Car[]> {
    const url = `${apiUrl}/${category}`;
    return this.http.get<Car[]>(url).pipe(
      tap(cars => console.log('Read cars by category')),
      catchError(this.handleError('getCars', []))
    );
  }

  getCheapestCar(dateStart: String, dateFinish: String,
     isLoyaltyProgram: boolean): Observable<Car> {
    const url = `${apiUrl}/${dateStart}/${dateFinish}/${isLoyaltyProgram}`;
    return this.http.get<Car>(url).pipe(
      tap(_ => console.log(`Found cheapest car id=${dateStart}`)),
      catchError(this.handleError<Car>(`getCheapestCar id=${dateStart}`))
    );
  }

  addCar (car): Observable<Car> {
    const url = `${apiUrl}`;
    return this.http.post<Car>(url, car, httpOptions).pipe(
      // tslint:disable-next-line:no-shadowed-variable
      tap((car: Car) => console.log(`Register a car w/ id=`)),
      catchError(this.handleError<Car>('addCar'))
    );
  }

  deleteCar (id): Observable<Car> {
    const url = `${apiUrl}/${id}`;

    return this.http.delete<Car>(url, httpOptions).pipe(
      tap(_ => console.log(`remove o produto com id=${id}`)),
      catchError(this.handleError<Car>('deleteCar'))
    );
  }
  
  private handleError<T> (operation = 'operation', result?: T) {
    return (error: any): Observable<T> => {

      console.error(error);

      return of(result as T);
    };
  }
}