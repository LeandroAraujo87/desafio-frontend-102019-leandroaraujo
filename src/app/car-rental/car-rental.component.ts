import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Component, OnInit } from '@angular/core';
import { ApiService } from './../../service/api.service';
import { Car } from 'src/model/car';

@Component({
  selector: 'app-car-rental',
  templateUrl: './car-rental.component.html',
  styleUrls: ['./car-rental.component.scss']
})
export class CarRentalComponent implements OnInit {
  displayedColumns: string[] = [ 'id', 'model', 'manufacturer', 'modelYear', 'category','action' ];
  listCategoories: string[] = [ 'COMPACT_HATCH' , 'MEDIUM_HATCH', 'SEDAN', 'VAN', 'PICKUP'];
  dataSource: Car[];
  isLoadingResults = true;
  formCategory: FormGroup;
  constructor(private _api: ApiService, private fb: FormBuilder) { }



  ngOnInit() {
    this.formCategory = this.fb.group({categorySelected: ['', Validators.required]});

    this._api.getCars()
    .subscribe(res => {
      this.dataSource = res;
      console.log(this.dataSource);
      this.isLoadingResults = false;
    }, err => {
      console.log(err);
      this.isLoadingResults = false;
    });
  }
  
  deleteCar(id: string){
    this.isLoadingResults = true;
    this._api.deleteCar(id)
      .subscribe(res => {
          this.isLoadingResults = false;
          this.ngOnInit();
        }, (err) => {
          console.log(err);
          this.isLoadingResults = false;
        });
  }

  findByCategory(category: number){
    this._api.getCarByCategory(category)
    .subscribe(res => {
      this.dataSource = res;
      console.log(this.dataSource);
      this.isLoadingResults = false;
    }, err => {
      console.log(err);
      this.isLoadingResults = false;
    });
    console.log(this.formCategory.controls.categorySelected.value);
    
  }

}
