import { Component, OnInit, ViewChild } from '@angular/core';
import { Router } from '@angular/router';
import { FormBuilder, FormGroup, NgForm, Validators } from '@angular/forms';
import { ApiService } from 'src/service/api.service';
import { timer } from 'rxjs';

@Component({
  selector: 'app-car-register',
  templateUrl: './car-register.component.html',
  styleUrls: ['./car-register.component.scss']
})
export class CarRegisterComponent implements OnInit {

  carForm: FormGroup;
  isLoadingResults = false;
  constructor(private router: Router, private api: ApiService, private formBuilder: FormBuilder) { }

  ngOnInit() {
    this.carForm = this.formBuilder.group({
   'model' : [null, Validators.required],
   'manufacturer' : [null, Validators.required],
   'modelYear' : [null, [Validators.required, Validators.maxLength(4)]],
   'category' : [null, Validators.required],
   'weekDayPrice' : [null, Validators.required],
   'weekendDayPrice' : [null, Validators.required],
   'weekDayPriceLoyalty' : [null, Validators.required],
   'weekendDayPriceLoyalty' : [null, Validators.required]
   
   });
  }
  addCar(form: NgForm) {
    this.isLoadingResults = true;
    this.api.addCar(form)
      .subscribe(res => {
          this.isLoadingResults = false;
          this.backToListCars();
        }, (err) => {
          console.log(err);
          this.isLoadingResults = false;
        });
  }

  backToListCars(){
    const source = timer(2000);
    this.isLoadingResults = false;
    source.subscribe(() => this.router.navigate(['/car-rental']));
  }


}
