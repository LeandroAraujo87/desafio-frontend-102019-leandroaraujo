import { Router, ActivatedRoute } from '@angular/router';
import { ApiService } from './../../service/api.service';
import { Car } from 'src/model/car';
import { Component, OnInit, ViewChild } from '@angular/core';
import { NgForm } from '@angular/forms';

@Component({
  selector: 'app-car-cheapest',
  templateUrl: './car-cheapest.component.html',
  styleUrls: ['./car-cheapest.component.scss']
})
export class CarCheapestComponent implements OnInit {


  
  car: Car;
  isLoadingResults = true;
 
    constructor(private apiService: ApiService,
                private router: Router,
                private activatedRoute: ActivatedRoute){}
  
  ngOnInit() {
    
  }

  getcheapestCar(dateStart: string, dateFinish: string , isLoyaltyProgram: boolean){
    this.apiService.getCheapestCar(dateStart, dateFinish, isLoyaltyProgram)
    .subscribe(res => {
      this.car = res;
      console.log(this.car);
      this.isLoadingResults = false;
    }, err => {
      console.log(err);
      this.isLoadingResults = false;
    });

     
  }
}
