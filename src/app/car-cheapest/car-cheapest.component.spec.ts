import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CarCheapestComponent } from './car-cheapest.component';

describe('CarCheapestComponent', () => {
  let component: CarCheapestComponent;
  let fixture: ComponentFixture<CarCheapestComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CarCheapestComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CarCheapestComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
