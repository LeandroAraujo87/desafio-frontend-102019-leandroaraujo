import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { CarRentalComponent } from './car-rental/car-rental.component';
import { CarRegisterComponent } from './car-register/car-register.component';
import { CarCheapestComponent } from './car-cheapest/car-cheapest.component';


const routes: Routes = [
  {
    path: 'car-rental',
    component: CarRentalComponent,
    data: { title: 'List of rental Cars' }
  },
  {
    path: 'car-register',
    component: CarRegisterComponent,
    data: { title: 'New Car' }
  },
  {
    path: 'car-cheapest',
    component: CarCheapestComponent,
    data: { title: 'Cheapest rental car' }
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
