export enum CategoriesEnum{
    COMPACT_HATCH = "1-Compact Hatch",
	MEDIUM_HATCH = "2-Medium Hatch",
    SEDAN = "3-Sedan",
	VAN = "4-Van",
	PICKUP = "5-Pickup"
}