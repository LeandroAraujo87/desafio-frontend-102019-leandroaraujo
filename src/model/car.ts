export interface Car {
    id: number;
    model: string;
    manufacturer: string;
    modelYear: number;
    category: number;
    weekDayPrice: number;
    weekendDayPrice: number;
    weekDayPriceLoyalty: number;
    weekendDayPriceLoyalty: number;
    totalRental: number;
  }